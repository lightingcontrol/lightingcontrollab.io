LightingControl
===============

Estimate of Version 1
---------------------

```mermaid
gantt
    title Plan for Version 1
    dateFormat YYYY-MM-DD
    section Frontend
        Channels     :channels, after fixtures, 10d
        Fixtures     :fixtures, after devices, 10d
        Devices      :devices, after scenes, 10d
        Scenes       :scenes, after shows, 20d
        Shows        :shows, 2023-08-14, 10d
    section Backend
```

Structure
---------

```
app
├── base          | Generic components which are used in multiple locations
│   └── header
├── home
├── objects       | Entities used for interaction with the backend
├── scene
├── service       | Code to interacti with the backend, also pure frontend services
├── setup
│   ├── channel
│   ├── device
│   └── fixture
└── show
```
