import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuItem } from 'primeng/api';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DropdownChangeEvent, DropdownModule } from 'primeng/dropdown';
import { MenubarModule } from 'primeng/menubar';
import { ProgressBarModule } from 'primeng/progressbar';

import { Backend, BackendService } from 'src/app/service/backend.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  standalone: true,
  imports: [
    CommonModule,
    BreadcrumbModule,
    DropdownModule,
    MenubarModule,
    ProgressBarModule
  ]
})
export class NavigationComponent implements OnInit {
  navigation: MenuItem[] | undefined;
  home: MenuItem | undefined;
  tooltipDelay = 650;
  loading: boolean = false;

  constructor(private backendService: BackendService) {
  }

  ngOnInit(): void {
    this.home = {
      icon: 'pi pi-home',
      routerLink: 'home',
      routerLinkActiveOptions: { exact: true },
      tooltipOptions: {
        tooltipLabel: 'No homepage available at the moment, will be there later.',
        disabled: false,
        showDelay: this.tooltipDelay,
      }
    };

    this.navigation = [
      {
        label: 'Setup',
        icon: 'pi pi-cog',
        routerLinkActiveOptions: { exact: true },
        tooltipOptions: {
          tooltipLabel: 'Before running any show, setup channels, fixtures and devices.',
          disabled: false,
          showDelay: this.tooltipDelay,
        },
        items: [
          {
            label: 'Devices',
            icon: 'pi pi-box',
            routerLink: 'devices',
            routerLinkActiveOptions: { exact: true },
            tooltipOptions: {
              tooltipLabel: 'Layout the devices in the available universes.',
              disabled: false,
              showDelay: this.tooltipDelay,
            }
          },
          {
            label: 'Fixtures',
            icon: 'pi pi-sun',
            routerLink: 'fixtures',
            routerLinkActiveOptions: { exact: true },
            tooltipOptions: {
              tooltipLabel: 'Fixtures define the different channels for a group of devices.',
              disabled: false,
              showDelay: this.tooltipDelay,
            }
          },
          {
            label: 'Channels',
            icon: 'pi pi-sliders-v',
            routerLink: 'channels',
            routerLinkActiveOptions: { exact: true },
            tooltipOptions: {
              tooltipLabel: 'Channels define the different types of channels like colors, movement, etc.',
              disabled: false,
              showDelay: this.tooltipDelay,
            }
          }
        ]
      },
      {
        label: 'Scenes',
        icon: 'pi pi-book',
        routerLink: 'scenes',
        routerLinkActiveOptions: { exact: true },
        tooltipOptions: {
          tooltipLabel: 'Single Scenes to be then combined to a Show.',
          disabled: false,
          showDelay: this.tooltipDelay,
        }
      },
      {
        label: 'Shows',
        icon: 'pi pi-clone',
        routerLink: 'shows',
        routerLinkActiveOptions: { exact: true },
        tooltipOptions: {
          tooltipLabel: 'The structure of a Show.',
          disabled: false,
          showDelay: this.tooltipDelay,
        }
      }
    ];
  }

  get backends(): Array<Backend> {
    return this.backendService.availableBackends ?? [];
  }

  selectBackend(event: DropdownChangeEvent) {
    this.backendService.chooseBackend(event.value);
  }

}
