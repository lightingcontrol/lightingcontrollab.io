import { Component, Input } from '@angular/core';

import { ChipModule } from 'primeng/chip';
import { Device } from 'src/app/objects/device';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  standalone: true,
  imports: [ChipModule]
})
export class DeviceComponent {
  @Input()
  device: Device = { name: '', fixture: {brand: '', channels: [], name: ''}, startAddress: 0 };
}
