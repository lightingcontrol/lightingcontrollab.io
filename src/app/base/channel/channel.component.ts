import { Component, Input } from '@angular/core';

import { ChipModule } from 'primeng/chip';

import { Channel } from 'src/app/objects/channel';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.scss'],
  standalone: true,
  imports: [
    ChipModule
  ]
})
export class ChannelComponent {
  @Input()
  channel: Channel = { name: '', type: '' };

  get channelClass(): string {
    return this.channel.name.replace(/[^A-Za-z0-9]+/g, '-').toLowerCase();
  }
}
