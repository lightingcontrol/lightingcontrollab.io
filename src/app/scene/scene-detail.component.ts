import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { FormArray, FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';

import { switchMap } from 'rxjs';

import { AutoCompleteCompleteEvent, AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipsModule } from 'primeng/chips';
import { DropdownModule } from 'primeng/dropdown';
import { KnobModule } from 'primeng/knob';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';

import { ChannelComponent } from '../base/channel/channel.component';
import { Effect } from '../objects/effect';
import { SceneService } from '../service/scene.service';
import { Channel } from '../objects/channel';
import { DeviceService } from '../service/device.service';
import { ChannelService } from '../service/channel.service';
import { Device } from '../objects/device';

@Component({
  templateUrl: './scene-detail.component.html',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,

    AutoCompleteModule,
    CheckboxModule,
    ChipsModule,
    DropdownModule,
    KnobModule,
    PanelModule,
    TableModule,

    ChannelComponent
  ]
})
export class SceneDetailComponent {
  loading: boolean = false;
  sceneForm = this.fb.group({
    name: ['', [Validators.required]],
    type: ['', [Validators.required]],
    values: this.fb.array([])
  })
  allDevices: Device[] = [];
  allChannels: Channel[] = [];
  suggestionDevice: string[] = [];
  suggestionChannel: string[] = [];

  constructor(
    private route: ActivatedRoute,
    private service: SceneService,
    private deviceService: DeviceService,
    private channelService: ChannelService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        this.loading = true;
        return this.service.getScene(params.get('name')!)
      })
    ).subscribe(scene => {
      this.sceneForm.patchValue(scene);

      for (const value of scene.values) {
        const sceneRow = this.createTableRow();
        this.valueArray.push(sceneRow);
        sceneRow.patchValue(value);
        // Devices
        for (let device in value.devices) {
          (sceneRow.controls['devices'] as FormArray).controls.push(this.fb.control(device));
        }
        sceneRow.controls["devices"].patchValue(value.devices);
        // Channels
        for (let channel in value.channels) {
          (sceneRow.controls['channels'] as FormArray).controls.push(this.fb.control(channel));
        }
        sceneRow.controls["channels"].patchValue(value.channels);
      }

      this.loading = false;
    });
    this.deviceService.getDevices().subscribe({
      next: devices => this.allDevices = devices.content
    });
    this.channelService.getChannels().subscribe({
      next: channels => this.allChannels = channels.content
    });
  }

  get valueArray(): FormArray {
    return this.sceneForm.controls["values"] as FormArray;
  }

  get effects(): Effect[] {
    return [
      Effect.STATIC,
      Effect.SINE,
      Effect.SQUARE,
      Effect.TRIANGLE,
      Effect.RAMP_UP,
      Effect.RAMP_DOWN
    ];
  }

  private createTableRow() {
    return this.fb.group({
      id: ['', [Validators.required]],
      devices: this.fb.array([]),
      channels: this.fb.array([]),
      min: [0, [Validators.min(0), Validators.max(255)]],
      max: [0, [Validators.min(0), Validators.max(255)]],
      type: ['', [Validators.required]],
      rate: [1, [Validators.min(0), Validators.max(16)]],
      offset: [0, [Validators.min(0), Validators.max(355)]],
      phase: [0, [Validators.min(0), Validators.max(355)]]
    });
  }

  onDeleteRow(valueIndex: number) {
    this.valueArray.removeAt(valueIndex);
  }

  searchDevice(event: AutoCompleteCompleteEvent) {
    this.suggestionDevice = this.allDevices
      .filter(device => device.name.startsWith(event.query))
      .map(device => device.name);
  }

  searchChannel(event: AutoCompleteCompleteEvent) {
    this.suggestionDevice = this.allChannels
      .filter(channel => channel.name.startsWith(event.query))
      .map(channel => channel.name);
  }

}
