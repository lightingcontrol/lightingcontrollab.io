import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { ScenesIndexComponent } from './scenes.component';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';

describe('ScenesIndexComponent', () => {
  let component: ScenesIndexComponent;
  let fixture: ComponentFixture<ScenesIndexComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [ScenesIndexComponent, NoopAnimationsModule],
    providers: [provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
});
    fixture = TestBed.createComponent(ScenesIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
