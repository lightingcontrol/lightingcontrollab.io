import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';

import { SceneInfo } from '../objects/scene';
import { Page } from '../objects/page';
import { SceneService } from '../service/scene.service';
import { ChannelComponent } from '../base/channel/channel.component';
import { DeviceComponent } from '../base/device/device.component';

@Component({
  templateUrl: './scenes.component.html',
  standalone: true,
  imports: [
    CommonModule, RouterModule,

    PanelModule, TableModule,

    ChannelComponent, DeviceComponent
  ]
})
export class ScenesIndexComponent {
  scenes: Page<SceneInfo> = { content: [], empty: true, first: true, last: true, numberOfElements: 0, pageable: { offset: 0, paged: false, pageNumber: 0, pageSize: 25, sort: { empty: true, sorted: true, unsorted: false }, unpaged: true }, size: 0, sort: { empty: true, sorted: false, unsorted: true }, totalElements: 0, totalPages: 0 }

  constructor(private sceneService: SceneService) {
  }

  ngOnInit(): void {
    this.sceneService.getScenes().subscribe((scenes) => this.scenes = scenes);
  }

}
