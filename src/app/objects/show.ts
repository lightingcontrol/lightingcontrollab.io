import { SceneInfo } from './scene';

export class Section {
    readonly id: string;
    readonly name: string;
    readonly bpm: number;
    readonly scenes: Array<SceneInfo>;

    constructor(id: string, name: string, bpm: number, scenes: Array<SceneInfo>) {
        this.id = id;
        this.name = name;
        this.bpm = bpm;
        this.scenes = scenes;
    }
}

export class ShowInfo {
    readonly name: string;
    readonly sections: number;

    constructor(name: string, sections: number) {
        this.name = name;
        this.sections = sections;
    }
}

export class Show {
    readonly name: string;
    readonly sections: Array<Section>;

    constructor(name: string, sections: Array<Section>) {
        this.name = name;
        this.sections = sections;
    }
}
