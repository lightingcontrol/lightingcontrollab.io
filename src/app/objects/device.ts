import { Fixture } from "./fixture";

export class Device {

    readonly name: string;
    readonly startAddress: number;
    readonly fixture: Fixture;

    constructor(name: string, startAddress: number, fixture: Fixture) {
        this.name = name;
        this.startAddress = startAddress;
        this.fixture = fixture;
    }

}