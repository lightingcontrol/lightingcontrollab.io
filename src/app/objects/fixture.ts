import { Channel } from "./channel";

export class Fixture {

    readonly name: string;
    readonly brand: string;
    readonly channels: Array<Channel>;

    constructor(name: string, brand: string, channels: Array<Channel>) {
        this.name = name;
        this.brand = brand;
        this.channels = channels;
    }

}