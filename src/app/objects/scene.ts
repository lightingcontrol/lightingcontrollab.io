import { Channel } from "./channel";
import { Device } from "./device";

export class Value {
    readonly id: string;
    readonly devices: Array<string>;
    readonly channels: Array<string>;
    readonly min: number;
    readonly max: number;
    readonly type: string;
    readonly rate: number;
    readonly offset: number;
    readonly phase: number;

    constructor(id: string, devices: Array<string>, channels: Array<string>, min: number, max: number, type: string, rate: number, offset: number, phase: number) {
        this.id = id;
        this.devices = devices;
        this.channels = channels;
        this.min = min;
        this.max = max;
        this.type = type;
        this.rate = rate;
        this.offset = offset;
        this.phase = phase;
    }
}

export class SceneInfo {
    readonly name: string;
    readonly type: string;
    readonly devices: Array<Device>;
    readonly channels: Array<Channel>;

    constructor(name: string, type: string, devices: Array<Device>, channels: Array<Channel>) {
        this.name = name;
        this.type = type;
        this.devices = devices;
        this.channels = channels;
    }
}

export class Scene {
    readonly name: string;
    readonly type: string;
    readonly values: Array<Value>;

    constructor(name: string, type: string, values: Array<Value>) {
        this.name = name;
        this.type = type;
        this.values = values;
    }
}