export enum Effect {

    STATIC = "static",
    SINE = "sine",
    SQUARE = "square",
    TRIANGLE = "triangle",
    RAMP_UP = "ramp-up",
    RAMP_DOWN = "ramp-down"

}