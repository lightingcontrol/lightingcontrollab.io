import { Component } from '@angular/core';

import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';

import { Channel } from 'src/app/objects/channel';
import { Page } from 'src/app/objects/page';
import { ChannelComponent } from 'src/app/base/channel/channel.component';
import { ChannelService } from 'src/app/service/channel.service';

@Component({
  templateUrl: './channels.component.html',
  imports: [
    PanelModule, TableModule,

    ChannelComponent
  ],
  standalone: true
})
export class ChannelsComponent {
  channels: Page<Channel> = { content: [], empty: true, first: true, last: true, numberOfElements: 0, pageable: { offset: 0, paged: false, pageNumber: 0, pageSize: 25, sort: { empty: true, sorted: true, unsorted: false }, unpaged: true }, size: 0, sort: { empty: true, sorted: false, unsorted: true }, totalElements: 0, totalPages: 0 }

  constructor(private channelService: ChannelService) {
  }

  ngOnInit(): void {
    this.channelService.getChannels().subscribe((channels) => this.channels = channels);
  }
}
