import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { FixturesIndexComponent } from './fixture.component';

describe('FixtureComponent', () => {
  let component: FixturesIndexComponent;
  let fixture: ComponentFixture<FixturesIndexComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FixturesIndexComponent, NoopAnimationsModule]
    });
    fixture = TestBed.createComponent(FixturesIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
