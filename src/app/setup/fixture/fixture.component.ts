import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';

import { Channel } from 'src/app/objects/channel';
import { Fixture } from 'src/app/objects/fixture';
import { ChannelComponent } from 'src/app/base/channel/channel.component';

@Component({
  templateUrl: './fixture.component.html',
  imports: [
    CommonModule,

    PanelModule, TableModule,

    ChannelComponent
  ],
  standalone: true
})
export class FixturesIndexComponent {
  private red = new Channel('red', 'color');
  private green = new Channel('green', 'color');
  private blue = new Channel('blue', 'color');
  private amber = new Channel('amber', 'color');
  private uv = new Channel('uv', 'color');
  private master = new Channel('master', 'color');
  private pan = new Channel('pan', 'position');
  private tilt = new Channel('tilt', 'position');

  fixtures: Array<Fixture> = [
    new Fixture('rgb', 'generic', [this.red, this.green, this.blue]),
    new Fixture('rgba', 'generic', [this.red, this.green, this.blue, this.amber]),
    new Fixture('rgbauv', 'generic', [this.red, this.green, this.blue, this.amber, this.uv]),
    new Fixture('rgbuv', 'generic', [this.red, this.green, this.blue, this.uv]),
    new Fixture('rgbm', 'generic', [this.red, this.green, this.blue, this.master]),
    new Fixture('rgbam', 'generic', [this.red, this.green, this.blue, this.amber, this.master]),
    new Fixture('pix8', 'generic', [
      this.red, this.green, this.blue, this.red, this.green, this.blue,
      this.red, this.green, this.blue, this.red, this.green, this.blue,
      this.red, this.green, this.blue, this.red, this.green, this.blue,
      this.red, this.green, this.blue, this.red, this.green, this.blue
    ]),
    new Fixture('scanrgb', 'generic', [this.pan, this.tilt, this.red, this.green, this.blue])
  ];
}
