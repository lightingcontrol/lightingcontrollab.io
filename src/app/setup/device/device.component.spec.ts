import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { DeviceIndexComponent } from './device.component';

describe('DeviceComponent', () => {
  let component: DeviceIndexComponent;
  let fixture: ComponentFixture<DeviceIndexComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DeviceIndexComponent, NoopAnimationsModule]
    });
    fixture = TestBed.createComponent(DeviceIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
