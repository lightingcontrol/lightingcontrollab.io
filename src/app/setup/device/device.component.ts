import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';

import { Channel } from 'src/app/objects/channel';
import { Device } from 'src/app/objects/device';
import { Fixture } from 'src/app/objects/fixture';
import { ChannelComponent } from 'src/app/base/channel/channel.component';
import { DeviceComponent } from 'src/app/base/device/device.component';

@Component({
  templateUrl: './device.component.html',
  imports: [
    CommonModule,

    PanelModule, TableModule,

    ChannelComponent, DeviceComponent
  ],
  standalone: true
})
export class DeviceIndexComponent {
  private red = new Channel('red', 'color');
  private green = new Channel('green', 'color');
  private blue = new Channel('blue', 'color');
  private amber = new Channel('amber', 'color');
  private uv = new Channel('uv', 'color');
  private pan = new Channel('pan', 'position');
  private tilt = new Channel('tilt', 'position');
  private rgb = new Fixture('rgb', 'generic', [this.red, this.green, this.blue]);
  private rgbauv = new Fixture('rgbauv', 'generic', [this.red, this.green, this.blue, this.amber, this.uv]);
  private pix8 = new Fixture('pix8', 'generic', [
    this.red, this.green, this.blue, this.red, this.green, this.blue,
    this.red, this.green, this.blue, this.red, this.green, this.blue,
    this.red, this.green, this.blue, this.red, this.green, this.blue,
    this.red, this.green, this.blue, this.red, this.green, this.blue
  ]);
  private scanrgb = new Fixture('scanrgb', 'generic', [this.pan, this.tilt, this.red, this.green, this.blue]);

  devices: Array<Device> = [
    new Device('par1', 0, this.rgb),
    new Device('par2', 3, this.rgb),
    new Device('par3', 6, this.rgb),
    new Device('par4', 9, this.rgb),
    new Device('bar1', 12, this.rgbauv),
    new Device('bar2', 17, this.rgbauv),
    new Device('bar3', 22, this.rgbauv),
    new Device('pix', 27, this.pix8),
    new Device('scan1', 51, this.scanrgb),
    new Device('scan2', 56, this.scanrgb)
  ];
}
