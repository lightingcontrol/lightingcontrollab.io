import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { BackendService } from './backend.service';
import { Page } from '../objects/page';
import { Show, ShowInfo } from '../objects/show';

@Injectable({
  providedIn: 'root'
})
export class ShowService {

  constructor(private httpClient: HttpClient, private backendService: BackendService) { }

  getShows(): Observable<Page<ShowInfo>> {
    return this.httpClient.get<Page<ShowInfo>>(this.baseUrl())
  }

  getShow(name: string): Observable<Show> {
    return this.httpClient.get<Show>(`${this.baseUrl()}/${name}`);
  }

  private baseUrl(): string {
    return `${this.backendService.backend.url}/api/show`;
  }
}
