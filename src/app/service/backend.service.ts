import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BackendService implements OnInit {
  availableBackends: Array<Backend> = [
    { name: "Demo", url: "https://minion2.asran.de" },
    { name: 'Local (dev)', url: 'http://localhost:8080' }
  ];
  backend: Backend = this.availableBackends[0];

  ngOnInit(): void {
    this.backend = JSON.parse(localStorage.getItem("lighting-control-backend") ?? JSON.stringify(this.backend));
    this.availableBackends = JSON.parse(localStorage.getItem("lighting-control-backends") ?? JSON.stringify(this.availableBackends));
  }

  chooseBackend(backend: Backend) {
    this.backend = backend;
  }

}

export interface Backend {
  name: string;
  url: string;
}
