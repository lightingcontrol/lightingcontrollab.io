import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { BackendService } from './backend.service';
import { Page } from '../objects/page';
import { Device } from '../objects/device';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(private httpClient: HttpClient, private backendService: BackendService) { }

  getDevices(): Observable<Page<Device>> {
    return this.httpClient.get<Page<Device>>(this.baseUrl())
  }

  getDevice(name: string): Observable<Device> {
    return this.httpClient.get<Device>(`${this.baseUrl()}/${name}`);
  }

  private baseUrl(): string {
    return `${this.backendService.backend.url}/api/device`;
  }
}
