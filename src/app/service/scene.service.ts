import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { BackendService } from './backend.service';
import { Page } from '../objects/page';
import { Scene, SceneInfo } from '../objects/scene';

@Injectable({
  providedIn: 'root'
})
export class SceneService {

  constructor(private httpClient: HttpClient, private backendService: BackendService) { }

  getScenes(): Observable<Page<SceneInfo>> {
    return this.httpClient.get<Page<SceneInfo>>(this.baseUrl())
  }

  getScene(name: string): Observable<Scene> {
    return this.httpClient.get<Scene>(`${this.baseUrl()}/${name}`);
  }

  private baseUrl(): string {
    return `${this.backendService.backend.url}/api/scene`;
  }
}
