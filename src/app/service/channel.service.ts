import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { BackendService } from './backend.service';
import { Page } from '../objects/page';
import { Channel } from '../objects/channel';

@Injectable({
  providedIn: 'root'
})
export class ChannelService {

  constructor(private httpClient: HttpClient, private backendService: BackendService) { }

  getChannels(): Observable<Page<Channel>> {
    return this.httpClient.get<Page<Channel>>(this.baseUrl())
  }

  getChannel(name: string): Observable<Channel> {
    return this.httpClient.get<Channel>(`${this.baseUrl()}/${name}`);
  }

  private baseUrl(): string {
    return `${this.backendService.backend.url}/api/channel`;
  }
}
