import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CommonModule } from '@angular/common'

import { switchMap } from 'rxjs';

import { ChipModule } from 'primeng/chip';
import { PanelModule } from 'primeng/panel'
import { TableModule } from 'primeng/table'

import { Show } from '../objects/show';
import { ShowService } from '../service/show.service';

@Component({
  templateUrl: './show-detail.component.html',
  standalone: true,
  imports: [
    CommonModule,

    ChipModule,
    PanelModule,
    TableModule
  ]
})
export class ShowDetailComponent implements OnInit {
  show: Show | undefined;
  loading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private service: ShowService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        this.loading = true;
        return this.service.getShow(params.get('name')!)
      })
    ).subscribe(show => {
      this.show = show;
      this.loading = false;
    })
  }

}
