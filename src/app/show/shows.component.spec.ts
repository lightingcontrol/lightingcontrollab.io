import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { ShowsIndexComponent } from './shows.component';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';

describe('ShowsIndexComponent', () => {
  let component: ShowsIndexComponent;
  let fixture: ComponentFixture<ShowsIndexComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [ShowsIndexComponent,
        NoopAnimationsModule],
    providers: [provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
});
    fixture = TestBed.createComponent(ShowsIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
