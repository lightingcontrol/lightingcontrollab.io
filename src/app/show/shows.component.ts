import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';

import { ShowInfo } from '../objects/show';
import { ShowService } from '../service/show.service'
import { Page } from '../objects/page';

@Component({
  templateUrl: './shows.component.html',
  standalone: true,
  imports: [
    RouterModule,

    PanelModule, TableModule,
  ]
})
export class ShowsIndexComponent implements OnInit {
  shows: Page<ShowInfo> = { content: [], empty: true, first: true, last: true, numberOfElements: 0, pageable: { offset: 0, paged: false, pageNumber: 0, pageSize: 25, sort: { empty: true, sorted: true, unsorted: false }, unpaged: true }, size: 0, sort: { empty: true, sorted: false, unsorted: true }, totalElements: 0, totalPages: 0 }

  constructor(private showService: ShowService) {
  }

  ngOnInit(): void {
    this.showService.getShows().subscribe((shows) => this.shows = shows);
  }

}
