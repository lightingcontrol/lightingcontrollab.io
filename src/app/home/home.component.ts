import { Component } from '@angular/core';

import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel';

@Component({
  templateUrl: './home.component.html',
  standalone: true,
  imports: [AccordionModule, ButtonModule, PanelModule]
})
export class HomeComponent {

}
