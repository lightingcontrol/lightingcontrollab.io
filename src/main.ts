import { provideHttpClient } from '@angular/common/http'
import { bootstrapApplication } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import { Route, provideRouter, withHashLocation } from '@angular/router';

import { AppComponent } from './app/app.component';

export const ROUTES: Route[] = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadComponent: () => import('./app/home/home.component').then(mod => mod.HomeComponent) },
  { path: 'channels', loadComponent: () => import('./app/setup/channel/channels.component').then(mod => mod.ChannelsComponent) },
  { path: 'fixtures', loadComponent: () => import('./app/setup/fixture/fixture.component').then(mod => mod.FixturesIndexComponent) },
  { path: 'devices', loadComponent: () => import('./app/setup/device/device.component').then(mod => mod.DeviceIndexComponent) },
  { path: 'scenes', loadComponent: () => import('./app/scene/scenes.component').then(mod => mod.ScenesIndexComponent) },
  { path: 'scene/:name', loadComponent: () => import('./app/scene/scene-detail.component').then(mod => mod.SceneDetailComponent) },
  { path: 'shows', loadComponent: () => import('./app/show/shows.component').then(mod => mod.ShowsIndexComponent) },
  { path: 'show/:name', loadComponent: () => import('./app/show/show-detail.component').then(mod => mod.ShowDetailComponent) },
];

bootstrapApplication(AppComponent, {
  providers: [
    provideHttpClient(),
    provideAnimations(),
    provideRouter(ROUTES, withHashLocation())
  ]
});